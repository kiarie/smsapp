@extends('layouts.web')
@section('content')
<h2>Compose Sms</h2>
  @include('web.error')
 <div class="form-horizontal">
 	<form action="{{route('sms.store')}}" method="POST">
 		{{ csrf_field() }}
 				<div class="form-group">
			<h4>Phone Number </h4>
			<input type="text" name="phone_number" placeholder="2547XXXXXXXX" value="{{old('phone_number')}}">
		</div>

		<div class="form-group">
			<h4>Message</h4>
			<textarea name="text_message" cols="50" rows="6">{{old('text_message')}}</textarea>
		</div>

		

		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-default btn-md"><i class="fa fa-pencil" aria-hidden="true"></i> Compose</button>
			</div>
		</div> 
	</form>                       		
</div>                           
@stop