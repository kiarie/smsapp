@extends('layouts.web')

@section('content')
 @include('web.error')
<?php if(!$texts->IsEmpty()):
$html='<table class="table table-hover">
					<thead>
						<tr>
							<th width="30%">Phone Number</th>
							<th width="10%">Status</th>
						</tr>
					</thead>
					<tbody>';
foreach($texts as $text):
$html.="<tr><td>$text->phone_number</td><td>$text->status_message</td></tr>";
endforeach;	
$html.="</tbody></table>";
endif;
$ft=time()."record.xls";
$file="../uploads/$ft";
$myfile = fopen($file, "w") or die("Unable to open file!");
$txt = $html;
fwrite($myfile, $txt);
fclose($myfile);
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<a href="<?php echo url("sms/create"); ?>" class="pull-right">
			<button class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> New Sms</button>
		</a>
		<a href="<?php echo url("sms/report/$ft"); ?>" class="pull-left">
			<button class="btn btn-default"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Report</button>
			</a>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@if(!$texts->IsEmpty()):
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="10%">Phone Number</th>
							<th width="10%">Status</th>
							<th width="10%">Date</th>
							<th width="20%">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						
				
			</div>
			@foreach($texts as $text)
			<tr>
							<td>{{$text->phone_number }}</td>
							<td>{{$text->status_message }}</td>
							<td>{{$text->created_at }}</td>
							<td>
							@if($text->status==null)	
								<a href="{{url("sms/send/$text->id")}}"><button class="btn btn-default"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Message</button></a>
							@endif	
								<a href="{{url("sms/$text->id")}}"><button class="btn btn-default"><i class="fa fa-chevron-right" aria-hidden="true"></i> Details</button></a>
							</td>
						</tr>
			@endforeach
				</tbody>
				</table>
		@else
		<h3>There are no texts at the moment.</h3>
		@endif
	</div>
</div>
@stop 