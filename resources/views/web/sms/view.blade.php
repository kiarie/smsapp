@extends('layouts.web')

@section('content')
  @include('web.error')
<h2>&rarr;Sms Details</h2>

<h4 class="txt-bold">Phone Number </h4>
<p>{{ $text->phone_number}}</p>
<h4 class="txt-bold">Message</h4>
<p>{{ $text->text_message}}</p>

<h4 class="txt-bold">Status : 
@if($text->status==1)
	Message Sent
@elseif($text->status==null)
	Message Pending
@elseif($text->status==2)
	Message Failed to send

@endif
</h4>
<a href="{{url("sms/send/$text->id")}}"><button class="btn btn-default"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send Message</button></a>
@stop