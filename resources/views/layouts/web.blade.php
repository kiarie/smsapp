<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name') }}</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
     <link href="{!!asset('css/web.css') !!}" rel="stylesheet" />
	</head>
	<body>

		<div class="container-fluid">
			<h1 class="brand-title text-center">Sms App</h1>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<ul class="list-group">
						<li class="list-group-item">
						<a href="<?php echo url("sms")?>">	<i class="fa fa-envelope-open" aria-hidden="true"></i> Sms</a>
						</li>
						<li class="list-group-item">
						<a href="<?php echo url("sms")?>">	<i class="fa fa-bar-chart" aria-hidden="true"></i> Report</a>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9">
					@yield('content') 
				</div>
			</div>
		</div>
<script src="//code.jquery.com/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
      $(document).ready(function() {
    // show the alert
    setTimeout(function() {
        $(".alert").alert('close');
    }, 2500);
});

		</script>
	</body>
</html>