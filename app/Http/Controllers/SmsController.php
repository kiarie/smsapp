<?php

namespace App\Http\Controllers;

use App\sms;
use Illuminate\Http\Request;
// use App\AfricasTalkingGateway as MyGateway;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $texts= sms::all();
        return view('web.sms.index',compact('texts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('web.sms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         // dd(request()->all());

        $sms= new sms;
         $this->validate($request, [
        'phone_number' => 'required|numeric',
        'text_message' => 'required|min:10',
        ]);

         $phone_number=Self::cleanTxt($request->phone_number);
         $text_message=Self::cleanTxt($request->text_message);
         echo strlen($phone_number);
         $sms->phone_number=$phone_number;
         $sms->text_message=$text_message;
         $sms->save();
         if($sms)
         {
            return redirect("/sms/".$sms->id)->with('success', 'Sms has been created, proced to send sms');
         }
         else{
            return redirect('/sms')->with('error', 'Try again an error occured.');
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        //
        $text= sms::find($id);
        return view('web.sms.view',compact('text'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function edit(sms $sms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sms $sms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function destroy(sms $sms)
    {
        //
    }

    public function report($file)
    {
        return response()->download(storage_path("../uploads/{$file}"));
    }

    public function send($id=null)
    {
         $sms= sms::find($id);  

         if($sms->status==1)
         {
            return redirect("/sms/".$sms->id)->with('success', 'Sms has been sent to '.$sms->phone_number);
         }
         else{
            Self::smsGateway($sms->phone_number,$sms->text_message,$sms->id);
         }
          return redirect()->back();
    }

    public function smsGateway($phone_number,$text_message,$id)
    {
 $sms= sms::find($id);       
require_once('AfricasTalkingGateway.php');
// Specify your authentication credentials
$username   = "int";
$apikey     = "bd2785794bd83efd62fa397bd5da9299ce594455d3b8180fdb36698ec1ed9bc2";
// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)
$recipients ="+".$phone_number;
// And of course we want our recipients to know what we really do
$message    = strip_tags(trim($text_message));
// Create a new instance of our awesome gateway class

$url="https://api.africastalking.com/restless/send?username=int&Apikey=bd2785794bd83efd62fa397bd5da9299ce594455d3b8180fdb36698ec1ed9bc2&to=$recipients&message=".urlencode($message);

// exit();
$filename = $url;
$ch = curl_init();
curl_setopt ($ch, CURLOPT_URL, $filename);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
$contents = curl_exec($ch);
curl_close($ch);

$searchWord ="Success";
$searchWord = preg_quote($searchWord);

if (strpos($contents,$searchWord) !== false) {
    $sms->status="1";
     $sms->status_message="success";
     $sms->save();
}
else{

    $sms->status="2";
     $sms->status_message="Failed";
     $sms->save();
}

    }

    public function cleanTxt($str=null)
    {
        $str=filter_var(trim($str),FILTER_SANITIZE_STRING);
        return $str;
    }
}
