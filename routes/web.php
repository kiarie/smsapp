<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SmsController@index');
Route::resource('sms','SmsController');
Route::get('sms/send/{id}','SmsController@send');
Route::get('sms/report/{file}','SmsController@report');
